package minesweeper;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Oliver
 */
public final class Tile extends StackPane {

	private Minesweeper parent;

	protected final int x, y;
	protected final boolean hasBomb;
	private boolean isOpen = false;
	private static final int TILE_SIZE = Minesweeper.TILE_SIZE;

	private final Rectangle border = new Rectangle(TILE_SIZE - 2, TILE_SIZE - 2);
	protected final Text text = new Text();
	protected FLAG flag = FLAG.NONE;

	public Tile(Minesweeper parent, int x, int y, boolean hasBomb) {
		this.parent = parent;
		this.x = x;
		this.y = y;
		this.hasBomb = hasBomb;

		border.setStroke(Color.LIGHTGRAY);
		border.setFill(Color.DARKGRAY);

		text.setFont(Font.font(18));
		text.setText(hasBomb ? "X" : "");
		text.setVisible(isOpen);

		getChildren().addAll(border, text);

		setTranslateX(x * TILE_SIZE);
		setTranslateY(y * TILE_SIZE);

		setOnMouseClicked(this::click);
	}

	private boolean isBlank() {
		return text.getText().isEmpty();
	}

	private void click(MouseEvent e) {
		switch (e.getButton()) {
			case PRIMARY:
				if (flag == FLAG.NONE) {
					open();
				}
				break;
			case SECONDARY:
				if (!isOpen) {
					cycleFlag();
				}
				break;
			default:
				//Do nothing
				break;
		}
	}

	public void cycleFlag() {
		switch (flag) {
			case NONE:
				setFlag(FLAG.MARK);
				break;
			case MARK:
				setFlag(FLAG.FLAG);
				break;
			case FLAG:
				setFlag(FLAG.NONE);
				break;
		}
	}

	private void setFlag(FLAG f) {
		this.flag = f;
		this.text.setVisible(flag.visibility);
		this.text.setText(flag.text);
	}

	public void open() {
		if (isOpen) {
			return;
		}

		if (hasBomb) {
			// TODO: make the game over better
			System.out.println("Game Over");
			parent.scene.setRoot(parent.createContent());
			return;
		}

		isOpen = true;
		text.setVisible(isOpen);
		border.setFill(null);

		// if it's not a bomb, and it's not next to a bomb,
		// open all of it's neighbours
		if (isBlank()) {
			parent.getNeighbours(this).forEach(Tile::open);
		}
	}

	private enum FLAG {
		NONE("", false), MARK("?", true), FLAG("\u2691", true);

		private final boolean visibility;
		private final String text;

		private FLAG(String text, boolean visibility) {
			this.text = text;
			this.visibility = visibility;
		}
	}
}
