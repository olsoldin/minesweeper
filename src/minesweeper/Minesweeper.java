/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Oliver
 */
public class Minesweeper extends Application {

	protected static final int TILE_SIZE = 40;
	private static final int W = 800;
	private static final int H = 600;
	private static final double BOMB_CHANCE = 0.2;

	private static final int X_TILES = W / TILE_SIZE;
	private static final int Y_TILES = H / TILE_SIZE;

	private final Tile[][] grid = new Tile[X_TILES][Y_TILES];
	protected Scene scene;

	protected Parent createContent() {
		Pane root = new Pane();
		root.setPrefSize(W, H);

		for (int y = 0; y < Y_TILES; y++) {
			for (int x = 0; x < X_TILES; x++) {
				Tile tile = new Tile(this, x, y, Math.random() < BOMB_CHANCE);

				grid[x][y] = tile;
				root.getChildren().add(tile);
			}
		}
		for (int y = 0; y < Y_TILES; y++) {
			for (int x = 0; x < X_TILES; x++) {
				Tile tile = grid[x][y];

				if (tile.hasBomb) {
					continue;
				}

				long bombs = getNeighbours(tile).parallelStream().filter(t -> t.hasBomb).count();

				if (bombs > 0) {
					tile.text.setText(String.valueOf(bombs));
				}
			}
		}
		return root;
	}

	protected List<Tile> getNeighbours(Tile tile) {
		List<Tile> neighbours = new ArrayList<>();

		// ttt
		// tXt
		// ttt
		int[] points = new int[]{
			-1, -1, // top left
			-1, 0, // left
			-1, 1, // bottom left
			0, -1, // top
			0, 1, // bottom
			1, -1, // top right
			1, 0, // right
			1, 1 // bottom right
		};

		for (int i = 0; i < points.length; i++) {
			int dx = points[i];
			int dy = points[++i];

			int newX = tile.x + dx;
			int newY = tile.y + dy;

			if (newX >= 0 && newX < X_TILES
					&& newY >= 0 && newY < Y_TILES) {
				neighbours.add(grid[newX][newY]);
			}
		}

		return neighbours;
	}

	@Override
	public void start(Stage stage) throws Exception {
		scene = new Scene(createContent());

		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
